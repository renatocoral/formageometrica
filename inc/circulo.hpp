#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica {
    public:
        Circulo();
        Circulo(float base);
        //~Circulo();
        
        void calculaArea();
		void calculaPerimetro();
		
};

#endif
